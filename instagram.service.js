angular.module("app").service("instagramService", function() {
  var listaFoto = [{
    image: "https://media.licdn.com/mpr/mpr/AAEAAQAAAAAAAAB6AAAAJDk5NDE0OWJiLTA3MTEtNDJjYS05NjgzLTgyZDJhMTM3NDE5Mg.jpg",
    nome: "paesaggioAlbero"
  },

  {
    image: "https://www.repstatic.it/content/nazionale/img/2015/11/26/121631321-63ff8328-4925-4ee2-9ff5-a81974b7074e.jpg",
    nome: "criceto"
  },

  {
    image: "http://cit.h-cdn.co/assets/16/17/980x490/landscape-1461536296-gettyimages-467604121.jpg",
    nome: "paesaggioBosco"
  },

  {
    image: "http://wallpaperart.altervista.org/Immagini/immagini_gattino_1024x768.jpg",
    nome: "gatto"
  },

  {
    image: "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSV4htpj_-wM49zhi-WqXvgpDgIxdW-NCGjm4bLWENEXkKJ5Wre2A",
    nome: "macchinaFotografica"
  }];

  var getLista = function() {
    return listaFoto;
  }

  return {
    getLista: getLista
  }
});
